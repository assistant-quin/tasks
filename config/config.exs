import Config

config :app, Tasks.Repo,
  database: "tasks",
  username: "user",
  password: "password",
  hostname: "db"

config :app,
  ecto_repos: [Tasks.Repo]
