FROM elixir:1.15-alpine

WORKDIR /app
COPY mix.exs mix.lock /app

RUN mix local.hex --force
RUN mix deps.get
