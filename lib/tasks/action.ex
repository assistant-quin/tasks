defmodule Tasks.Action do
  @moduledoc """
    Use this module to make actions that can be plugged into
    the router easily.

    ## Examples:

    iex> defmodule SimpleAction do
    ...>   use Tasks.Action
    ...>
    ...>   def run(_) do
    ...>     {:ok, "ok"}
    ...>   end
    ...> end
    iex> SimpleAction.call()
    {:ok, "ok"}

    ## Input validation

    You can override the changeset/1 method to validate the input using Ecto changesets.

    You can add an embedded_schema to the module for this changeset validation or use
    one from another module's Ecto schema.

    ### Example:

    iex> defmodule ActionWithValidation do
    ...>   use Tasks.Action
    ...>
    ...>   embedded_schema do
    ...>     field(:bool, :boolean)
    ...>   end
    ...>
    ...>   def changeset(input) do
    ...>     %__MODULE__{}
    ...>     |> Ecto.Changeset.cast(input, [:bool])
    ...>     |> Ecto.Changeset.validate_required([:bool])
    ...>   end
    ...> end
    iex> ActionWithValidation.call(%{})
    {:malformed_data, [bool: {"can't be blank", [validation: :required]}]}
    iex> ActionWithValidation.call(%{bool: true})
    {:ok, %{}}
  """
  @moduledoc since: "0.1.0"

  defmodule Behaviour do
    @moduledoc false

    @callback run(changeset :: %Ecto.Changeset{}) ::
                {:ok, answer :: map}
                | {:malformed_data, changeset :: %Ecto.Changeset{}}
                | {:error, term}

    @callback changeset(attrs :: map) :: %Ecto.Changeset{}
  end

  defmacro __using__(_) do
    quote do
      use Ecto.Schema
      import Ecto.Changeset

      @behaviour Behaviour

      def run(_), do: {:ok, %{}}
      def changeset(_), do: %Ecto.Changeset{valid?: true, changes: %{}}

      defoverridable Behaviour

      def validate(attrs) do
        case changeset(attrs) do
          %Ecto.Changeset{valid?: false} = changeset -> {:malformed_data, changeset.errors}
          %Ecto.Changeset{valid?: true} = changeset -> {:ok, changeset}
        end
      end

      def call(attrs \\ %{}) do
        case validate(attrs) do
          {:ok, changeset} -> run(changeset)
          {:malformed_data, errors} -> {:malformed_data, errors}
        end
      end
    end
  end
end
