defmodule Tasks.Router do
  use Plug.Router

  plug(Plug.Logger)

  plug(:match)

  plug(Plug.Parsers,
    parsers: [:json],
    pass: ["application/json"],
    json_decoder: Jason
  )

  plug(:dispatch)

  post "/tasks" do
    Tasks.Actions.Tasks.Creation
    |> run_action(conn)
    |> answer(conn)
  end

  get "/tasks" do
    Tasks.Actions.Tasks.List
    |> run_action(conn)
    |> answer(conn)
  end

  get "/_health" do
    {:ok, "OK"} |> answer(conn)
  end

  match _ do
    {:not_found, "Not found"} |> answer(conn)
  end

  defp answer(response, conn) do
    %{code: code, message: message} =
      case response do
        {:ok, message} -> %{code: 200, message: message}
        {:created, message} -> %{code: 201, message: message}
        {:not_found, message} -> %{code: 404, message: message}
        {:malformed_data, message} -> %{code: 400, message: message}
        {:server_error, _} -> %{code: 500, message: "An error occurred internally"}
      end

    send_resp(conn, code, message)
  end

  defp run_action(action, conn) do
    with {:ok, changes} <- action.validate(conn.body_params),
         {code, result} <- action.run(changes) do
      {code, Jason.encode!(result)}
    end
  end
end
