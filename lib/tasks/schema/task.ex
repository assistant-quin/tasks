defmodule Tasks.Task do
  @moduledoc """
  Schema for tasks.

  Primary key: `uuid` (binary id)

  Required fields:
  - `name`

  Defaults:
  - `done`: `false`
  """
  @moduledoc since: "0.1.0"

  use Tasks.Schema

  @derive {Jason.Encoder, only: [:uuid, :name, :done]}
  schema "tasks" do
    timestamps()

    field(:name, :string)
    field(:done, :boolean, default: false)
  end

  @doc """
  Creates a changeset for a task.

  ## Examples:

    iex> Tasks.Task.changeset(%Tasks.Task{}, %{name: "foobar"})
    #Ecto.Changeset<action: nil, changes: %{name: "foobar"}, errors: [], data: #Tasks.Task<>, valid?: true>

    iex> Tasks.Task.changeset(%Tasks.Task{}, %{})
    #Ecto.Changeset<action: nil, changes: %{}, errors: [name: {"can't be blank", [validation: :required]}], data: #Tasks.Task<>, valid?: false>
  """
  @doc since: "0.1.0"
  def changeset(task, params \\ %{}) do
    task
    |> Ecto.Changeset.cast(params, [:name, :done])
    |> Ecto.Changeset.validate_required([:name])
  end
end
