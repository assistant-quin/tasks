defmodule Tasks.Actions.Tasks.List do
  @moduledoc """
  Action to list tasks.

  ## Examples:

  iex> name1 = "Name %{Ecto.UUID.generate()}"
  iex> name2 = "Name %{Ecto.UUID.generate()}"
  iex> Tasks.Actions.Tasks.Creation.call(%{name: name1})
  iex> Tasks.Actions.Tasks.Creation.call(%{name: name2})
  iex> {:ok, tasks} = Tasks.Actions.Tasks.List.call()
  iex> Enum.any?(tasks, &(&1.name == name1)) && Enum.any?(tasks, &(&1.name == name2))
  true
  """
  @moduledoc since: "0.1.0"
  use Tasks.Action

  def run(_) do
    {:ok, Tasks.Repo.all(Tasks.Task)}
  end
end
