defmodule Tasks.Actions.Tasks.Creation do
  @moduledoc """
  Action to create tasks.

  ## Examples:

    iex> {:created, task} = Tasks.Actions.Tasks.Creation.call(%{name: "foobar"})
    iex> task.name
    "foobar"
    iex> task.done
    false

    iex> Tasks.Actions.Tasks.Creation.call(%{})
    iex> {:malformed_data, [name: {"can't be blank", [validation: :required]}]}
  """
  @moduledoc since: "0.1.0"

  use Tasks.Action

  def run(changeset) do
    with {:ok, task} <- Tasks.Repo.insert(changeset) do
      {:created, task}
    end
  end

  def changeset(attrs) do
    Tasks.Task.changeset(%Tasks.Task{}, attrs)
  end
end
