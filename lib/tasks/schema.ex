defmodule Tasks.Schema do
  @moduledoc """
  Use this module instead of Ecto.Schema to automatically set
  the primary and foreign key types properly to UUIDs.
  """
  defmacro __using__(_) do
    quote do
      use Ecto.Schema
      @primary_key {:uuid, :binary_id, autogenerate: true}
      @foreign_key_type :binary_id
    end
  end
end
