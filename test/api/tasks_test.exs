defmodule TasksTest.Api.Tasks do
  use ExUnit.Case, async: true
  use Plug.Test

  @opts Tasks.Router.init([])

  test "post /tasks -> creates a task" do
    conn = conn(:post, "/tasks", %{name: "foobar"})
    resp = call(conn)
    task = resp_body(resp)

    assert resp.status == 201
    assert task["name"] == "foobar"
    assert task["uuid"]
    assert task["done"] == false
  end

  test "get /tasks -> lists all tasks" do
    name1 = "Name %{Ecto.UUID.generate()}"
    name2 = "Name %{Ecto.UUID.generate()}"
    Tasks.Actions.Tasks.Creation.call(%{name: name1})
    Tasks.Actions.Tasks.Creation.call(%{name: name2})

    conn = conn(:get, "/tasks")
    resp = call(conn)
    tasks = resp_body(resp)

    assert resp.status == 200
    assert tasks |> length >= 2
    assert Enum.any?(tasks, &(&1["name"] == name1))
    assert Enum.any?(tasks, &(&1["name"] == name2))
  end

  defp resp_body(resp) do
    Jason.decode!(resp.resp_body)
  end

  defp call(conn) do
    Tasks.Router.call(conn, @opts)
  end
end
