defmodule TasksTest.Router do
  use ExUnit.Case, async: true
  use Plug.Test

  @opts Tasks.Router.init([])

  test "returns ok" do
    conn = conn(:get, "/_health")
    conn = Tasks.Router.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body == "OK"
  end
end
