defmodule TasksTest.Task do
  use ExUnit.Case, async: true
  doctest Tasks.Task

  test "can be created" do
    {:ok, task} = create_task(%{name: "foobar"})

    assert task.uuid
    assert task.done == false
    assert task.name == "foobar"
  end

  test "can't create task without a name" do
    {:error, changeset} = create_task(%{})
    assert changeset.errors[:name]
    refute changeset.valid?
  end

  defp create_task(params) do
    Tasks.Task.changeset(%Tasks.Task{}, params)
    |> Tasks.Repo.insert()
  end
end
