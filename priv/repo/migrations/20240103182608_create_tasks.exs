defmodule Tasks.Repo.Migrations.CreateTasks do
  use Ecto.Migration

  def change do
    create table("tasks", primary_key: false) do
      add :uuid, :uuid, primary_key: true
      timestamps()

      add :name, :string, null: false
      add :done, :boolean, null: false, default: false
    end
  end
end
