# Tasks

Service that manages tasks.

## Setup & tests

```bash
docker-compose build
docker-compose run --rm api mix test
```
